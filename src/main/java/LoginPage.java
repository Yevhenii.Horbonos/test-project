import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class LoginPage {
    public WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }



    public WebElement waitForElementVisibility(String xpath) {
        return new WebDriverWait(driver, Duration.of(60, ChronoUnit.SECONDS))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
    }



    public void fillAndClearLoginFields(String username, String password) {

        WebElement usernameInput = driver.findElement(By.xpath("//input[@id='login-username']"));
        WebElement passwordInput = driver.findElement(By.xpath("//input[@id='login-password']"));

        usernameInput.sendKeys(username);
        passwordInput.sendKeys(password);


        clearField(usernameInput);
        clearField(passwordInput);

    }

    public void login(String username, String password) {
        WebElement usernameInput = driver.findElement(By.xpath("//input[@id='login-username']"));
        WebElement passwordInput = driver.findElement(By.xpath("//input[@id='login-password']"));
        WebElement loginButton = driver.findElement(By.xpath("//button[@id='login-button']"));

        usernameInput.sendKeys(username);
        passwordInput.sendKeys(password);

        loginButton.click();

    }

    private void clearField(WebElement field) {
        while (!field.getAttribute("value").isEmpty()) {
            field.sendKeys(Keys.BACK_SPACE);
        }

    }

    public String getUsernameErrorMessage() {

        WebElement errorElement = waitForElementVisibility("//div[@id='username-error']//p[contains(@class,'Type')]");


        return errorElement.getText();
    }

    public String getPasswordErrorMessage() {
        WebElement errorElement = waitForElementVisibility("//div[@id='password-error']//span[contains(@class,'Text')]");

        return errorElement.getText();
    }

    public String getErrorMessage() {

        WebElement errorElement = waitForElementVisibility("//div[@data-encore-id='banner']//span");

        return errorElement.getText();
    }

}