import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ProfilePage extends LoginPage {

    public ProfilePage(WebDriver driver) {
        super(driver);
    }


    public void profile() {
        WebElement account = waitForElementVisibility("//button[@data-testid='user-widget-link']");

        account.click();
        WebElement profile = waitForElementVisibility("//li//span[contains(text(),'Профиль')]");

        profile.click();
    }

    public String getProfileName() {
        WebElement errorElement = waitForElementVisibility("//button//h1");
        return errorElement.getText();
    }

}
